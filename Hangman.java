import java.util.Scanner;


public class Hangman{

	public static void main (String[] args){

		Scanner reader = new Scanner(System.in);
		
		//user input for a 4 letters word
		System.out.println("Enter a 4 letter word that would have to be guessed! ");
		String word = reader.nextLine();
		
		//start hangman game
		runGame(word);
		
	}

	public static int isLetterInWord(String word, char c){
		//verify if the guessed char is in the word and return its position
		for(int i = 0; i < word.length(); i++){
			if( toUpperCase( word.charAt(i) ) == toUpperCase(c) ){
				return i;
			}
		}
		return -1;
	}
	
	public static char toUpperCase(char c){
		//lower case char transformed to upper case
		c = Character.toUpperCase(c);
		return c;
	}
	
	public static void printWord(String word, boolean[] isRightLetter){
		//declaration of the variable countaining the message
		String guessedWord = "";

		//if the letter as been found, add the letter to the message presenting the found letters
		for(int i = 0; i < isRightLetter.length; i++){
			if(isRightLetter[i]){
				guessedWord += word.charAt(i);
			}
			
			//if not found, add a dash to signifie that it as not been found
			else{
			guessedWord += "-";
			}
		}
			
		System.out.println(guessedWord);
	}
	
	public static void runGame(String word){
		
		Scanner reader = new Scanner(System.in);
		
		//declaration of the main variables that will affect the victory state
		boolean[] isRightLetter = new boolean[4];
		boolean victory = false;
		int numRightLetters = 0;
		int numMisses = 0;
		
		
		//game loop until a loss or victory state is reached
		while(!victory && numMisses < 6){
			
			//Prompt user to input a letter used to guess the mystery word
			System.out.println("Try to guess a letter! Enter your choice in the command line.");
			char guess = reader.nextLine().charAt(0);

			//verify if the letter is in the word and give the char position
			int letterPos = isLetterInWord(word, guess);
			
			//if the guessed letter is not found in the mystery word
			if (letterPos < 0){
				numMisses++;
				System.out.println("You made a mistake!Now you only have " + (6 - numMisses) + " chances left, be careful!");
			}
			//if found, add +1 to numRightLetters (4 = victory)
			else{
				System.out.println("Your guess was right! Continue like that and you might win this game!");
				
				for(int i = 0; i< isRightLetter.length; i++){
					if(letterPos == i){
						if (!isRightLetter[i]){
							isRightLetter[i] = true;
							numRightLetters += 1;
						}	
					}
				}
			}
			
		
			//victory state
			if(numRightLetters == 4){
				victory = true;
				}
			
			//Summary of the letters found until now
			System.out.println("Letter(s) found: " );
			printWord(word, isRightLetter);
				
		}
		
		//Victory or loss message
		if(victory){
			System.out.println("Bravo, You WON the game!");
		}
		else{
			System.out.println("Too bad, you LOST the game!");
		}
	}
}